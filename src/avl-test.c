/* Copyright (C) 2019 Mike Petersen */
/*
 * This file is part of avl.
 *
 * Avl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Avl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with avl.  If not, see <https://www.gnu.org/licenses/>.
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include <avl.h>


enum
operation_type
{
  OP_TYPE_INSERT,
  OP_TYPE_FIND,
  OP_TYPE_REMOVE
};


static
int
_strcmp_wrapper(void const *l, void const *r, void *arg);

static
int
_strchoice(char const *str, int choice_count, ...);

static
int
_height(struct avl_node const *root);

static
void
_check_weights(struct avl_node const *root);

static
void
_print_structure(struct avl_node const *root, size_t indent);

static
int
_print_string(void *datum, void *arg);


int
main(int argc, char **argv)
{
  struct avl t;
  enum operation_type op;
  int i, tmp;
  char *found;


  if (avl_init(&t, &_strcmp_wrapper, NULL))
  {
    return 1;
  }

  op = OP_TYPE_INSERT;
  for (i = 1; i < argc; ++i)
  {
    tmp = _strchoice(argv[i], 8, "-i", "-f", "-r", "-ps", "-tpre", "-tin",
                     "-tpost", "-cw");
    if (-1 != tmp)
    {
      switch (tmp)
      {
        case 0:
          op = OP_TYPE_INSERT;
          break;
        case 1:
          op = OP_TYPE_FIND;
          break;
        case 2:
          op = OP_TYPE_REMOVE;
          break;
        case 3:
          _print_structure(t.root, 0);
          break;
        case 4:
          tmp = 1;
          avl_preorder(&t, &_print_string, &tmp);
          printf("\n");
          break;
        case 5:
          tmp = 1;
          avl_inorder(&t, &_print_string, &tmp);
          printf("\n");
          break;
        case 6:
          tmp = 1;
          avl_postorder(&t, &_print_string, &tmp);
          printf("\n");
          break;
        case 7:
          _check_weights(t.root);
          break;
      }

      continue;
    }

    switch (op)
    {
      case OP_TYPE_INSERT:
        tmp = avl_insert(&t, argv[i]);
        printf("insert(\"%s\") -> %d\n", argv[i], tmp);
        break;
      case OP_TYPE_FIND:
        tmp = avl_find(&t, argv[i], (void **) &found);
        printf("find(\"%s\") -> %d\n", argv[i], tmp);
        if (!tmp)
        {
          printf("  found \"%s\"\n", found);
        }
        break;
      case OP_TYPE_REMOVE:
        tmp = avl_remove(&t, argv[i], (void **) &found);
        printf("remove(\"%s\") -> %d\n", argv[i], tmp);
        if (!tmp)
        {
          printf("  removed \"%s\"\n", found);
        }
        break;
    }
  }

  avl_destroy(&t, NULL, NULL);

  return 0;
}


/*******************
 * local functions *
 *******************/


int
_strcmp_wrapper(void const *l, void const *r, void *arg)
{
  (void) arg;

  return strcmp((char const *) l, (char const *) r);
}

int
_strchoice(char const *str, int choice_count, ...)
{
  char const *choice;
  int i;
  va_list choices;


  va_start(choices, choice_count);

  for (i = 0; i < choice_count; ++i)
  {
    choice = va_arg(choices, char const *);

    if (0 == strcmp(str, choice))
    {
      return i;
    }
  }

  va_end(choices);

  return -1;
}

int
_height(struct avl_node const *root)
{
  int l, r;


  if (NULL == root)
  {
    return 0;
  }

  l = _height(root->left);
  r = _height(root->right);

  return 1 + ((l > r) ? l : r);
}

void
_check_weights(struct avl_node const *root)
{
  int l, r;


  if (NULL == root)
  {
    return;
  }

  l = _height(root->left);
  r = _height(root->right);

  if (root->weight != r - l)
  {
    printf("found incorrect weight, was %d, heights were r=%d, l=%d, r-l=%d\n",
           root->weight, r, l, r - l);
  }

  _check_weights(root->left);
  _check_weights(root->right);
}

void
_print_structure(struct avl_node const *root, size_t indent)
{
  size_t i;


  for (i = 0; i < indent; ++i)
  {
    printf("  ");
  }
  printf(">");

  if (NULL == root)
  {
    printf("\n");
  }
  else
  {
    printf(" \"%s\" (w=%d,h=%d)\n", (char const *) root->datum, root->weight,
           _height(root));

    ++indent;
    if (NULL != root->left || NULL != root->right)
    {
      _print_structure(root->left, indent);
      _print_structure(root->right, indent);
    }
  }
}

int
_print_string(void *datum, void *arg)
{
  int *first;


  first = arg;

  if (*first)
  {
    printf("\"%s\"", (char const *) datum);
    *first = 0;
  }
  else
  {
    printf(" \"%s\"", (char const *) datum);
  }

  return 0;
}
