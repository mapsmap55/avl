/* Copyright (C) 2019 Mike Petersen */
/*
 * This file is part of avl.
 *
 * Avl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Avl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with avl.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef AVL_H
# define AVL_H


# include <stddef.h>


# ifdef __cplusplus
extern "C"
{
# endif


enum
avl_weight
{
  AVL_WEIGHT_LEFT_HEAVY = -1,
  AVL_WEIGHT_NEUTRAL,
  AVL_WEIGHT_RIGHT_HEAVY
};

struct
avl_node
{
  enum avl_weight
  weight;

  void *
  datum;

  struct avl_node *
  parent;

  struct avl_node *
  left;

  struct avl_node *
  right;
};

struct
avl
{
  struct avl_node *
  root;

  size_t
  size;

  int
  (*comparator)(void const *, void const *, void *);

  void *
  comparator_arg;
};


int
avl_init(struct avl *t, int (*comparator)(void const *, void const *, void *),
         void *comparator_arg);

void
avl_destroy(struct avl *t, void (*datum_destroy)(void *, void *), void *arg);

int
avl_insert(struct avl *t, void *datum);

int
avl_find(struct avl *t, void const *datum, void **store);

# define avl_contains(t, datum) (0 == avl_find((t), (datum), NULL))

int
avl_remove(struct avl *t, void const *datum, void **store);

size_t
avl_preorder(struct avl *t, int (*visit)(void *, void *), void *arg);

size_t
avl_inorder(struct avl *t, int (*visit)(void *, void *), void *arg);

size_t
avl_postorder(struct avl *t, int (*visit)(void *, void *), void *arg);


# ifdef __cplusplus
}
# endif


#endif
