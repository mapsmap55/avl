/* Copyright (C) 2019 Mike Petersen */
/*
 * This file is part of avl.
 *
 * Avl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Avl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with avl.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "avl.h"

#include <stdlib.h>


#define HAS_PARENT(n) (NULL != (n)->parent)
#define HAS_LEFT(n) (NULL != (n)->left)
#define HAS_RIGHT(n) (NULL != (n)->right)
#define IS_LEFT(n) (HAS_PARENT(n) && (n) == (n)->parent->left)
#define IS_RIGHT(n) (HAS_PARENT(n) && (n) == (n)->parent->right)
#define IS_INNER(n) (HAS_LEFT(n) && HAS_RIGHT(n))


enum
avl_traverse_state
{
  AVL_TRAVERSE_STATE_PRE,
  AVL_TRAVERSE_STATE_IN,
  AVL_TRAVERSE_STATE_POST
};

struct
avl_traverse_destroy_args
{
  void
  (*datum_destroy)(void *, void *);

  void *
  arg;
};

struct
avl_traverse_visit_wrapper_args
{
  int
  (*visit)(void *, void *);

  void *
  arg;

  size_t
  count;
};


static
struct avl_node *
_avl_find_last(struct avl *t, void const *datum, int *last_cmp);

static
void
_avl_traverse(struct avl *t, enum avl_traverse_state visit_state,
              int (*visit)(struct avl_node *, void *), void *arg);

static
int
_avl_traverse_destroy(struct avl_node *node, void *arg);

static
int
_avl_traverse_visit_wrapper(struct avl_node *node, void *arg);

static
void
_avl_node_rotate_left(struct avl_node *node);

static
void
_avl_node_rotate_right(struct avl_node *node);

static
void
_avl_rebalance_insert(struct avl *t, struct avl_node *inserted);

static
void
_avl_rebalance_remove(struct avl *t, struct avl_node *removed,
                      struct avl_node *replacement, int was_left);


int
avl_init(struct avl *t, int (*comparator)(void const *, void const *, void *),
         void *comparator_arg)
{
  t->root = NULL;
  t->size = 0;
  t->comparator = comparator;
  t->comparator_arg = comparator_arg;

  return 0;
}

void
avl_destroy(struct avl *t, void (*datum_destroy)(void *, void *), void *arg)
{
  struct avl_traverse_destroy_args args;


  args.datum_destroy = datum_destroy;
  args.arg = arg;
  _avl_traverse(t, AVL_TRAVERSE_STATE_POST, &_avl_traverse_destroy, &args);
}

int
avl_insert(struct avl *t, void *datum)
{
  struct avl_node *found, *new_node;
  int last_cmp;


  found = _avl_find_last(t, datum, &last_cmp);
  if (NULL != found && 0 == last_cmp)
  {
    return 1;
  }

  new_node = malloc(sizeof(*new_node));
  if (NULL == new_node)
  {
    return 2;
  }

  new_node->weight = AVL_WEIGHT_NEUTRAL;
  new_node->datum = datum;
  new_node->parent = found;
  new_node->left = NULL;
  new_node->right = NULL;

  if (NULL == found)
  {
    t->root = new_node;
  }
  else
  {
    if (0 > last_cmp)
    {
      found->left = new_node;
    }
    else
    {
      found->right = new_node;
    }
  }

  ++t->size;

  _avl_rebalance_insert(t, new_node);

  return 0;
}

int
avl_find(struct avl *t, void const *datum, void **store)
{
  struct avl_node *found;
  int last_cmp;


  found = _avl_find_last(t, datum, &last_cmp);
  if (NULL == found || 0 != last_cmp)
  {
    return 1;
  }

  if (NULL != store)
  {
    *store = found->datum;
  }

  return 0;
}

int
avl_remove(struct avl *t, void const *datum, void **store)
{
  struct avl_node *found, *tmp;
  int last_cmp, was_left;


  found = _avl_find_last(t, datum, &last_cmp);
  if (NULL == found || 0 != last_cmp)
  {
    return 1;
  }

  if (NULL != store)
  {
    *store = found->datum;
  }

  if (IS_INNER(found))
  {
    tmp = found->right;
    while (HAS_LEFT(tmp))
    {
      tmp = tmp->left;
    }
    found->datum = tmp->datum;
    found = tmp;
  }

  was_left = IS_LEFT(found);

  tmp = (HAS_LEFT(found)) ? found->left : found->right;

  if (!HAS_PARENT(found))
  {
    t->root = tmp;
  }
  else
  {
    if (IS_LEFT(found))
    {
      found->parent->left = tmp;
    }
    else
    {
      found->parent->right = tmp;
    }
  }

  if (NULL != tmp)
  {
    tmp->parent = found->parent;
  }

  --t->size;

  _avl_rebalance_remove(t, found, tmp, was_left);

  free(found);

  return 0;
}

size_t
avl_preorder(struct avl *t, int (*visit)(void *, void *), void *arg)
{
  struct avl_traverse_visit_wrapper_args args;


  args.visit = visit;
  args.arg = arg;
  args.count = 0;
  _avl_traverse(t, AVL_TRAVERSE_STATE_PRE, &_avl_traverse_visit_wrapper, &args);

  return args.count;
}

size_t
avl_inorder(struct avl *t, int (*visit)(void *, void *), void *arg)
{
  struct avl_traverse_visit_wrapper_args args;


  args.visit = visit;
  args.arg = arg;
  args.count = 0;
  _avl_traverse(t, AVL_TRAVERSE_STATE_IN, &_avl_traverse_visit_wrapper, &args);

  return args.count;
}

size_t
avl_postorder(struct avl *t, int (*visit)(void *, void *), void *arg)
{
  struct avl_traverse_visit_wrapper_args args;


  args.visit = visit;
  args.arg = arg;
  args.count = 0;
  _avl_traverse(t, AVL_TRAVERSE_STATE_POST, &_avl_traverse_visit_wrapper,
                &args);

  return args.count;
}


/*******************
 * local functions *
 *******************/


struct avl_node *
_avl_find_last(struct avl *t, void const *datum, int *last_cmp)
{
  struct avl_node *curr, *last;
  int c;


  last = NULL;
  curr = t->root;
  c = 0;

  while (NULL != curr)
  {
    last = curr;

    c = (*t->comparator)(datum, curr->datum, t->comparator_arg);
    if (0 == c)
    {
      break;
    }

    if (0 > c)
    {
      curr = curr->left;
    }
    else
    {
      curr = curr->right;
    }
  }

  *last_cmp = c;

  return (NULL != curr) ? curr : last;
}

void
_avl_traverse(struct avl *t, enum avl_traverse_state visit_state,
              int (*visit)(struct avl_node *, void *), void *arg)
{
  struct avl_node *curr_node, *next_node;
  enum avl_traverse_state curr_state, next_state;


  curr_node = t->root;
  curr_state = AVL_TRAVERSE_STATE_PRE;

  while (NULL != curr_node)
  {
    switch (curr_state)
    {
      case AVL_TRAVERSE_STATE_PRE:
        if (HAS_LEFT(curr_node))
        {
          next_node = curr_node->left;
          next_state = AVL_TRAVERSE_STATE_PRE;
        }
        else
        {
          next_node = curr_node;
          next_state = AVL_TRAVERSE_STATE_IN;
        }
        break;
      case AVL_TRAVERSE_STATE_IN:
        if (HAS_RIGHT(curr_node))
        {
          next_node = curr_node->right;
          next_state = AVL_TRAVERSE_STATE_PRE;
        }
        else
        {
          next_node = curr_node;
          next_state = AVL_TRAVERSE_STATE_POST;
        }
        break;
      case AVL_TRAVERSE_STATE_POST:
        next_node = curr_node->parent;
        next_state = (IS_LEFT(curr_node))
                       ? AVL_TRAVERSE_STATE_IN
                       : AVL_TRAVERSE_STATE_POST;
        break;
    }

    if (curr_state == visit_state && (*visit)(curr_node, arg))
    {
      break;
    }

    curr_node = next_node;
    curr_state = next_state;
  }
}

int
_avl_traverse_destroy(struct avl_node *node, void *arg)
{
  struct avl_traverse_destroy_args *args;


  args = arg;

  if (NULL != args->datum_destroy)
  {
    (*args->datum_destroy)(node->datum, args->arg);
  }

  free(node);

  return 0;
}

int
_avl_traverse_visit_wrapper(struct avl_node *node, void *arg)
{
  struct avl_traverse_visit_wrapper_args *args;


  args = arg;

  ++args->count;

  return (*args->visit)(node->datum, args->arg);
}

void
_avl_node_rotate_left(struct avl_node *node)
{
  struct avl_node *c, *p, *g;


  c = node->left;
  p = node->parent;
  g = p->parent;

  if (NULL != g)
  {
    if (IS_LEFT(p))
    {
      g->left = node;
    }
    else
    {
      g->right = node;
    }
  }
  node->parent = g;

  node->left = p;
  p->parent = node;

  p->right = c;
  if (NULL != c)
  {
    c->parent = p;
  }
}

void
_avl_node_rotate_right(struct avl_node *node)
{
  struct avl_node *c, *p, *g;


  c = node->right;
  p = node->parent;
  g = p->parent;

  if (NULL != g)
  {
    if (IS_LEFT(p))
    {
      g->left = node;
    }
    else
    {
      g->right = node;
    }
  }
  node->parent = g;

  node->right = p;
  p->parent = node;

  p->left = c;
  if (NULL != c)
  {
    c->parent = p;
  }
}

void
_avl_rebalance_insert(struct avl *t, struct avl_node *inserted)
{
  struct avl_node *n, *p, *c;
  int done;


  n = inserted;
  p = n->parent;

  done = 0;
  while (!done && NULL != p)
  {
    if (n == p->left)
    {
      switch (p->weight)
      {
        case AVL_WEIGHT_LEFT_HEAVY:
          /*
           *      P
           *     / \
           *    N   -
           *   / \
           *  + ^ +
           */
          if (AVL_WEIGHT_RIGHT_HEAVY == n->weight)
          {
            /*
             *      P
             *     / \
             *    N   -
             *   / \
             *  -   +
             */
            c = n->right;
            /*
             *      P
             *     / \
             *    N   -
             *   / \
             *  -   C
             *     / \
             *    - | -
             */
            _avl_node_rotate_left(c);
            _avl_node_rotate_right(c);
            /*
             *       C
             *     /   \
             *    N     P
             *   / \   / \
             *  -   -|-   -
             */
            n->weight = (AVL_WEIGHT_RIGHT_HEAVY == c->weight)
                          ? AVL_WEIGHT_LEFT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;
            p->weight = (AVL_WEIGHT_LEFT_HEAVY == c->weight)
                          ? AVL_WEIGHT_RIGHT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;
            c->weight = AVL_WEIGHT_NEUTRAL;

            if (p == t->root)
            {
              t->root = c;
            }

            done = 1;
          }
          else
          {
            /*
             *      P
             *     / \
             *    N   -
             *   / \
             *  +   -
             */
            _avl_node_rotate_right(n);
            /*
             *    N
             *   / \
             *  +   P
             *     / \
             *    -   -
             */
            n->weight = AVL_WEIGHT_NEUTRAL;
            p->weight = AVL_WEIGHT_NEUTRAL;

            if (p == t->root)
            {
              t->root = n;
            }

            done = 1;
          }
          break;
        case AVL_WEIGHT_NEUTRAL:
          /*
           *      P
           *     / \
           *    N   -
           *   / \
           *  - | -
           */
          /* unbalances the tree within tolerance */
          p->weight = AVL_WEIGHT_LEFT_HEAVY;
          /* height has changed, not done yet */
          break;
        case AVL_WEIGHT_RIGHT_HEAVY:
          /*
           *      P
           *     / \
           *    N   +
           *   / \
           *  -   -
           */
          /* balances the tree */
          p->weight = AVL_WEIGHT_NEUTRAL;
          done = 1;
          break;
      }
    }
    else
    {
      switch (p->weight)
      {
        case AVL_WEIGHT_RIGHT_HEAVY:
          if (AVL_WEIGHT_LEFT_HEAVY == n->weight)
          {
            c = n->left;

            _avl_node_rotate_right(c);
            _avl_node_rotate_left(c);

            n->weight = (AVL_WEIGHT_LEFT_HEAVY == c->weight)
                          ? AVL_WEIGHT_RIGHT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;
            p->weight = (AVL_WEIGHT_RIGHT_HEAVY == c->weight)
                          ? AVL_WEIGHT_LEFT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;
            c->weight = AVL_WEIGHT_NEUTRAL;

            if (p == t->root)
            {
              t->root = c;
            }

            done = 1;
          }
          else
          {
            _avl_node_rotate_left(n);

            n->weight = AVL_WEIGHT_NEUTRAL;
            p->weight = AVL_WEIGHT_NEUTRAL;

            if (p == t->root)
            {
              t->root = n;
            }

            done = 1;
          }
          break;
        case AVL_WEIGHT_NEUTRAL:
          p->weight = AVL_WEIGHT_RIGHT_HEAVY;
          break;
        case AVL_WEIGHT_LEFT_HEAVY:
          p->weight = AVL_WEIGHT_NEUTRAL;
          done = 1;
          break;
      }
    }

    n = p;
    p = n->parent;
  }
}

void
_avl_rebalance_remove(struct avl *t, struct avl_node *removed,
                      struct avl_node *replacement, int was_left)
{
  struct avl_node *n, *p, *s, *sc;
  int done, flag, first;


  n = replacement;
  p = removed->parent;

  first = 1;

  done = 0;
  while (!done && NULL != p)
  {
    if ((first && was_left) || (!first && n == p->left))
    {
      switch (p->weight)
      {
        case AVL_WEIGHT_LEFT_HEAVY:
          /*
           * was
           *      P
           *     / \
           *    N   -
           *   / \
           *  - | -
           */
          /*
           * now
           *      P
           *     / \
           *    N   -
           *   / \
           *  --|--
           */
          p->weight = AVL_WEIGHT_NEUTRAL;
          /* not done; height still changes farther up the tree */
          break;
        case AVL_WEIGHT_NEUTRAL:
          /*
           * was
           *      P
           *     / \
           *    N   +
           *   / \
           *  - | -
           */
          /*
           * now
           *      P
           *     / \
           *    N   +
           *   / \
           *  --|--
           */
          s = p->right;
          /*
           *       P
           *     /   \
           *    N     S
           *   / \   / \
           *  --|-- - | -
           */
          if (AVL_WEIGHT_LEFT_HEAVY == s->weight)
          {
            /*
             *       P
             *     /   \
             *    N     S
             *   / \   / \
             *  --|-- -  --
             */
            p->weight = AVL_WEIGHT_RIGHT_HEAVY;
            /*
             * height has not changed farther up the tree
             * (if "-" is 1, was 3 before, is 3 now)
             */
            done = 1;
          }
          else
          {
            _avl_node_rotate_left(s);
            /*
             *        S
             *       / \
             *      P   -
             *     / \
             *    N  -/--
             *   / \
             *  --|--
             */
            p->weight = (AVL_WEIGHT_RIGHT_HEAVY == s->weight)
                          ? AVL_WEIGHT_LEFT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;
            s->weight = AVL_WEIGHT_LEFT_HEAVY;

            if (p == t->root)
            {
              t->root = s;
            }
            /*
             * height has not changed farther up the tree
             * (if "-" is 1, was 3 before, is 3 now)
             */
            done = 1;
          }
          break;
        case AVL_WEIGHT_RIGHT_HEAVY:
          /*
           * was
           *      P
           *     / \
           *    N  ++
           *   / \
           *  - | -
           */
          /*
           * now
           *      P
           *     / \
           *    N  ++
           *   / \
           *  --|--
           */
          s = p->right;
          /*
           *       P
           *     /   \
           *    N     S
           *   / \   / \
           *  --|-- + | +
           */
          if (AVL_WEIGHT_LEFT_HEAVY == s->weight)
          {
            /*
             *       P
             *     /   \
             *    N     S
             *   / \   / \
             *  --|-- +   -
             */
            sc = s->left;
            /*
             *       P
             *     /   \
             *    N     S
             *   / \   / \
             *  --|-- SC  -
             *       / \
             *      - | -
             */
            _avl_node_rotate_right(sc);
            _avl_node_rotate_left(sc);
            /*
             *         SC
             *       /   \
             *      P     S
             *     / \   / \
             *    N   -|-   -
             *   / \
             *  --|--
             */
            p->weight = (AVL_WEIGHT_RIGHT_HEAVY == sc->weight)
                          ? AVL_WEIGHT_LEFT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;
            s->weight = (AVL_WEIGHT_LEFT_HEAVY == sc->weight)
                          ? AVL_WEIGHT_RIGHT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;
            sc->weight = AVL_WEIGHT_NEUTRAL;

            if (p == t->root)
            {
              t->root = sc;
            }

            /* not done; height still changes farther up the tree */
            /* need to set N and P so next iteration is correct */
            n = p;
            p = sc;
          }
          else
          {
            /*
             *        P
             *     /     \
             *    N       S
             *   / \     / \
             *  --|--  +/-  +
             */
            _avl_node_rotate_left(s);
            /*
             *        S
             *       / \
             *      P   +
             *     / \
             *    N  +/-
             *   / \
             *  --|--
             */
            flag = AVL_WEIGHT_NEUTRAL == s->weight;
            p->weight = (flag)
                          ? AVL_WEIGHT_RIGHT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;
            s->weight = (flag)
                          ? AVL_WEIGHT_LEFT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;

            if (p == t->root)
            {
              t->root = s;
            }

            /*
             * height has not changed farther up the tree
             * (if "-" is 1, was 4 before, is 4 (if S was neutral)
             * or 3 (if S was right-heavy) now)
             */
            done = flag;
            /* need to set N and P so next (possible) iteration is correct */
            n = p;
            p = s;
          }
          break;
      }
    }
    else
    {
      switch (p->weight)
      {
        case AVL_WEIGHT_RIGHT_HEAVY:
          p->weight = AVL_WEIGHT_NEUTRAL;
          break;
        case AVL_WEIGHT_NEUTRAL:
          s = p->left;

          if (AVL_WEIGHT_RIGHT_HEAVY == s->weight)
          {
            p->weight = AVL_WEIGHT_LEFT_HEAVY;

            done = 1;
          }
          else
          {
            _avl_node_rotate_right(s);

            p->weight = (AVL_WEIGHT_LEFT_HEAVY == s->weight)
                          ? AVL_WEIGHT_RIGHT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;
            s->weight = AVL_WEIGHT_RIGHT_HEAVY;

            if (p == t->root)
            {
              t->root = s;
            }

            done = 1;
          }
          break;
        case AVL_WEIGHT_LEFT_HEAVY:
          s = p->left;

          if (AVL_WEIGHT_RIGHT_HEAVY == s->weight)
          {
            sc = s->right;

            _avl_node_rotate_left(sc);
            _avl_node_rotate_right(sc);

            p->weight = (AVL_WEIGHT_LEFT_HEAVY == sc->weight)
                          ? AVL_WEIGHT_RIGHT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;
            s->weight = (AVL_WEIGHT_RIGHT_HEAVY == sc->weight)
                          ? AVL_WEIGHT_LEFT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;
            sc->weight = AVL_WEIGHT_NEUTRAL;

            if (p == t->root)
            {
              t->root = sc;
            }

            n = p;
            p = sc;
          }
          else
          {
            _avl_node_rotate_right(s);

            flag = AVL_WEIGHT_NEUTRAL == s->weight;
            p->weight = (flag)
                          ? AVL_WEIGHT_LEFT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;
            s->weight = (flag)
                          ? AVL_WEIGHT_RIGHT_HEAVY
                          : AVL_WEIGHT_NEUTRAL;

            if (p == t->root)
            {
              t->root = s;
            }

            done = flag;

            n = p;
            p = s;
          }
          break;
      }
    }

    first = 0;

    n = p;
    p = n->parent;
  }
}
